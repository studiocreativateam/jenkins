![jenkins](./jenkins.png)

# Jenkins + Docker

## Setup
1. Ask me [Jacek Labudda](mailto:j.labudda@creativa.studio?subject=Problem%20o%20z%20jenkinsem) if you have a problem
2. Create project `composer create-project studiocreativateam/jenkins`
3. Run command `make up`

#### Usefully commands
- Docker start: `make up` or `make up-win`
- Docker stop: `make down` or `make down-win`
- Go do bash: `make bash` or `make bash-win`
