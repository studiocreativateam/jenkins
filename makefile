up:
	docker-compose up -d
	docker exec -ti jenkins sh -c "service jenkins start"
up-win:
	docker-compose up -d
	winpty docker exec -ti jenkins sh -c "service jenkins start"
down:
	docker-compose down
down-win:
	make down
bash:
	docker exec -it jenkins /bin/bash
bash-win:
	winpty docker exec -it ${CONTAINER_NAME_APP} bash
update:
	git add --all
	git reset --hard
	git pull